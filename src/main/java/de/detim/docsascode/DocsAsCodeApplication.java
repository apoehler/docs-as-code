package de.detim.docsascode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DocsAsCodeApplication {

    public static void main(String[] args) {
        SpringApplication.run(DocsAsCodeApplication.class, args);
    }

}
